// Copyright Epic Games, Inc. All Rights Reserved.

#include "CraftInteractiveTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CraftInteractiveTest, "CraftInteractiveTest" );
