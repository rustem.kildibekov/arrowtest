// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CraftInteractiveTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CRAFTINTERACTIVETEST_API ACraftInteractiveTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
